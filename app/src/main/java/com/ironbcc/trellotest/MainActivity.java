package com.ironbcc.trellotest;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * TODO: Add destription
 *
 * @author ironbcc on 19.01.16.
 */
public class MainActivity extends Activity implements View.OnClickListener {
    public static final String DICTIONARY = "acdegilmnoprstuw";
    public static final int START_HASH_STATE = 7;
    public static final int BIT = 37;
    public static final int START_HASH_BIT = START_HASH_STATE * BIT;
    EditText chars;
    EditText hash;
    TextView hashResult;
    TextView unhashResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        chars = (EditText) findViewById(R.id.chars);
        hash = (EditText) findViewById(R.id.hash);
        hashResult = (TextView) findViewById(R.id.hashResult);
        unhashResult = (TextView) findViewById(R.id.unhashResult);
        findViewById(R.id.calculateBtn).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        try {
            final Editable charsText = chars.getText();
            final Editable hashText = hash.getText();
            if(TextUtils.isEmpty(charsText) && TextUtils.isEmpty(hashText)) {
                showNotification(getString(R.string.notification_text));
            } if(!TextUtils.isEmpty(hashText)) {
                try {
                    unhashResult.setText(
                        getString(
                            R.string.computed_unhash,
                            unhash(Long.parseLong(hashText.toString())
                            )
                    ));
                } catch (NumberFormatException nfe) {
                    showNotification(getString(R.string.notification_hash_error));
                }
            } else {
                final long calculate = hash(charsText.toString());
                hashResult.setText(getString(R.string.computed_hash, calculate));
                unhashResult.setText(getString(R.string.computed_unhash, unhash(calculate)));
            }
        } catch (IllegalArgumentException e) {
            showNotification(e.getMessage());
        }
    }

    private void showNotification(CharSequence text) {
        hashResult.setText(text);
        unhashResult.setText(null);
    }

    private long hash(String s) {
        long h = START_HASH_STATE;
        for(int i = 0; i < s.length(); i++) {
            final int charIdx = DICTIONARY.indexOf(s.charAt(i));
            if(charIdx < 0) {
                throw new IllegalArgumentException(getString(R.string.error_text, DICTIONARY));
            }
            h = (h * BIT + charIdx);
        }
        return h;
    }

    private String unhash(long hash) {
        if(hash < START_HASH_BIT) return null;
        StringBuilder builder = new StringBuilder();
        while (hash >= START_HASH_BIT) {
            final long tmp = (hash / BIT);
            final int idx = (int) (hash - tmp * BIT);
            builder.append(DICTIONARY.charAt(idx));
            hash = (hash - idx) / BIT;
        }
        return builder.reverse().toString();
    }
}
